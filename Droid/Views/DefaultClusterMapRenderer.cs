﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;
using Com.Google.Maps.Android.Clustering;
using Com.Google.Maps.Android.Clustering.View;

namespace ClusteringMapXamarinForms.Droid
{
	public class DefaultClusterMapRenderer : DefaultClusterRenderer
	{
		
		public DefaultClusterMapRenderer(Context Context, Android.Gms.Maps.GoogleMap GoogleMap, ClusterManager clustermanager)
			: base(Context, GoogleMap, clustermanager)
		{

		}

		protected override void OnBeforeClusterItemRendered(Java.Lang.Object item, MarkerOptions markerOptions)
		{

			//IClusterItem myItem = item as ClusterItem;

			markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.Drops));
				

			base.OnBeforeClusterItemRendered(item, markerOptions);
		}

		/*protected override void OnBeforeClusterRendered(ICluster cluster, MarkerOptions markerOptions)
		{

			markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.clustermarker));
			base.OnBeforeClusterRendered(cluster, markerOptions);
		}*/
	
			protected override void OnClusterRendered(ICluster cluster, Marker markerOptions)
		{
			//int height = 100;
			//int width = 100;
			//Drawable icon = Resources.GetDrawable(Resource.Drawable.clustermarker);
			markerOptions.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.clustermarker));
			markerOptions.Title = cluster.Size.ToString();
			base.OnClusterRendered(cluster, markerOptions);
		}

		private Bitmap scaleImage(Resources res, int id, int lessSideSize)
		{
			Bitmap b = null;
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.InJustDecodeBounds = true;
			BitmapFactory.DecodeResource(res, id, o);
			float sc = 0.0f;
			int scale = 1;
			// if image height is greater than width
			if (o.OutHeight > o.OutWidth)
			{
				sc = o.OutHeight / lessSideSize;
				scale = (int)Math.Round(sc);
			}
			// if image width is greater than height
			else {
				sc = o.OutWidth / lessSideSize;
				scale = (int)Math.Round(sc);
			}
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.InSampleSize = scale;
			b = BitmapFactory.DecodeResource(res, id, o2);
			return b;
		}

	}
}
