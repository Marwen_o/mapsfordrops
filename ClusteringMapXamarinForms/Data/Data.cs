﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms.Maps;

namespace ClusteringMapXamarinForms
{
	public class Data
	{
		public Collection<CustomPinForAndroid> Items { get; set; }
		public Data()
		{
			Items = new Collection<CustomPinForAndroid>();
			var Latitude = 40.741895;
			var Longitude = -73.989308;

			for (int i = 0; i < 5; ++i)
			{
				var CurrentPin = new CustomPinForAndroid();
				var t = i * System.Math.PI * 0.33f;
				var r = 0.005 * System.Math.Exp(0.1 * t);
				var x = r * System.Math.Cos(t);
				var y = r * System.Math.Sin(t);
				CurrentPin.Id = "Xamarin";
				CurrentPin.Pin = new Pin
				{
					Position = new Position(Latitude + x, Longitude + y),
					Label = "likwid San Francisco Office",
					Address = "lac 1 ,cogite"
				};
				CurrentPin.Url = i.ToString();

				Items.Add(CurrentPin);
			}
		}
	}
}