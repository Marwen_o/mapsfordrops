﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClusteringMapsXamarinForms;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace ClusteringMapXamarinForms
{
	public partial class CustomMapAndroid : ContentPage
	{
		public CustomMapForAndroid customMap { get; set; }
		public Grid Main = new Grid();
		public CustomMapAndroid()
		{
			InitializeComponent();
			Data Data = new Data();

			customMap = new CustomMapForAndroid()
			{
				MapType = MapType.Street,
				WidthRequest = App.ScreenWidh,
				HeightRequest = App.ScreenHeigh,
				Parameter = 0
			};
			customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(36.83543436404748, 10.238987484454494), Distance.FromMiles(1.0)));
			customMap.CustomPins = Data.Items.ToList();
			//foreach (var item in customMap.CustomPins)
			//{
			//	customMap.Pins.Add(item.Pin);
			//}
			Main.Children.Add(customMap);
			Content = Main;

		}
	}
}
