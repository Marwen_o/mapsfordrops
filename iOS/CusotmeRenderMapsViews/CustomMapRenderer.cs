﻿using System;
using ClusteringMapXamarinForms;
using ClusteringMapXamarinForms.iOS;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.GoogleMaps.iOS;
using Google.Maps;
using Xamarin.Forms.Maps;
using UIKit;
using System.Collections.Generic;
using GMCluster;
using Foundation;
using CoreGraphics;
using CoreAnimation;
using System.Drawing;
using ClusteringMapXamarinForms.CustomFormElements;
using CoreFoundation;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
//[assembly: ExportRendererAttribute(typeof(ModalHostPage), typeof(ModalHostPageRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
	public class CustomMapRenderer : MapRenderer, IGMUClusterRendererDelegate, IGMUClusterManagerDelegate, IMapViewDelegate

	{
		//UIView customPinView;
		//List<CustomPin> customPins;
		const int kClusterItemCount = 1000;
		const double kCameraLatitude = 41.894708;
		const double kCameraLongitude = 12.492523;
        const double extent = 0.2;
		MapView map;
		public int i = 0;

		public static Details dtPage = null;
		GMUClusterManager clusterManager;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{

			base.OnElementChanged(e);
			if (e.OldElement != null)
			{
				map = Control as MapView;
				map.UserInteractionEnabled = true;
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;
			
			}

			if (e.NewElement != null)
			{
				var formsMap = (CustomMap)e.NewElement;
				map = Control as MapView;

				map.UserInteractionEnabled = true;
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;
			
			}
		}
			
		public UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{ 
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;}

     void AddCluster ()
    {
      var googleMapView = map;
      var iconGenerator = new GMUDefaultClusterIconGenerator ();
      var algorithm = new GMUNonHierarchicalDistanceBasedAlgorithm ();
      var renderer = new GMUDefaultClusterRenderer (googleMapView, iconGenerator);
    
       renderer.WeakDelegate = this;
    
      clusterManager = new GMUClusterManager (googleMapView, algorithm, renderer);
    
       for (var i = 0; i <= kClusterItemCount; i++)
       {
          var lat = kCameraLatitude + extent * GetRandomNumber (-1.0, 1.0);
        
          var lng = kCameraLongitude + extent * GetRandomNumber (-1.0, 1.0);
        
          var name = $"Item {i}";
        
          IGMUClusterItem item = new POIItem (lat, lng, name);
        
          clusterManager.AddItem (item);
        }
    
    		clusterManager.Cluster ();
    
  		    clusterManager.SetDelegate (this, this);
}


		public double GetRandomNumber(double minimum, double maximum)
		{
			Random random = new Random();
			return random.NextDouble() * (maximum - minimum) + minimum;
		}


		[Export("renderer:willRenderMarker:")]
		public void WillRenderMarker(GMUClusterRenderer renderer, Overlay marker)
		{
			
			
			if (marker is Marker)
			{
				var myMarker = (Marker)marker;
				UIImage clusterMarker = UIImage.FromBundle("clustermarker");
				clusterMarker = ResizeImage(clusterMarker, 50,70);
				myMarker.Icon = clusterMarker;


				if (myMarker.UserData is POIItem)
				{
					//If we need to resize the drop

				UIImage pic1 = UIImage.FromBundle("pic1");
						pic1 = ResizeImage(pic1, 60,65);
					UIImage pic2 = UIImage.FromBundle("pic2");
					pic2 = ResizeImage(pic2, 60, 65);
					UIImage pic3 = UIImage.FromBundle("pic3");
					pic3 = ResizeImage(pic3, 60, 65);

					POIItem item = (POIItem)myMarker.UserData;
					myMarker.Title = "Challenge";
					if (i == 0)
					{
						myMarker.Icon = pic1;
					}
					else if (i%2==0)
					{
						myMarker.Icon = pic2;
					}
					else 
					{
						myMarker.Icon = pic3;
					}
				
					myMarker.Tappable = true;

				}

			}
			renderer.Update();
			i++;
		}


		[Export("mapView:didTapMarker:")]
		public bool TappedMarker(MapView mapView, Marker marker)
		{

			return true;
		}


		[Export("clusterManager:didTapCluster:")]
		public void DidTapCluster(GMUClusterManager clusterManager, IGMUCluster cluster)
		{
			var newCamera = CameraPosition.FromCamera(cluster.Position, map.Camera.Zoom + 30);

			var update = CameraUpdate.SetCamera(newCamera);
			clusterManager.Cluster();
			map.MoveCamera(update);
		}

	

	}
}
